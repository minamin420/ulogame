﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace ServerUlo
{
    class Server
    {
        const string IP_ADDRESS = "127.0.0.1";
        const int PORT = 5000;
        private static string[] User = { "User1", "User2", "User3" };
        private static string[] Pass = { "Pass1", "Pass2", "Pass3" };
        private static Dictionary<int, User> UserList = new Dictionary<int, User>();
        private static Dictionary<int, TcpClient> ClientList = new Dictionary<int, TcpClient>();
        private static IFormatter formatter = new BinaryFormatter();
        private static int clientCounter;

        private static void UloServer(object argument)
        {
            int ID = clientCounter;
            TcpClient client = (TcpClient)argument;
            NetworkStream networkStream = client.GetStream();
            ClientList.Add(ID, client);
            ConnectionHandler(client);
            StreamReader reader = new StreamReader(client.GetStream());
            try
            {

                while (client!=null)
                {
                    string fromClient = reader.ReadLine();
                    Console.WriteLine(fromClient);
                    if (fromClient.Contains("Up"))
                    {
                        MovePlayerUp(client);
                    } 
                    else if (fromClient.Contains("Down"))
                    {
                        MovePlayerDown(client);
                    }
                    else if (fromClient.Contains("Left"))
                    {
                        MovePlayerLeft(client);
                    }
                    else if (fromClient.Contains("Right"))
                    {
                        MovePlayerRight(client);
                    }
                    
                }
                reader.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Client Disconnected");
            }
        }
        public static bool ValidateUserData(User user)
        {
            if (user != null)
            {
                for (int i = 0; i < User.Length; i++)
                {
                    if (user.Username == User[i] && user.Password == Pass[i])
                    {
                        UserList.Add(clientCounter, user);
                        return true;
                    }
                }
                return false;
            }
            else
            {
                Console.WriteLine("Account not Exist");
                return false;
            }
        }

        public static void BroadCast(string data)
        {
            foreach (KeyValuePair<int, TcpClient> client in ClientList)
            {
                StreamWriter sw = new StreamWriter(client.Value.GetStream());
                sw.WriteLine(data);
                sw.Flush();
            }
        }

        static void ConnectionHandler(TcpClient client)
        {
            NetworkStream networkStream = client.GetStream();

            formatter.Binder = new CustomizedBinder();
            User user = (User)formatter.Deserialize(networkStream);
            Console.WriteLine(user.Username);
            if (ValidateUserData(user))
            {
                Console.WriteLine("User Logged In : " + user.Username);
                BroadCast(user.Username);
            }

            else
            {
                Console.WriteLine("Connection Error / Account didn't exist");
            }
        }

        public static void MovePlayerUp(TcpClient client)
        {
            StreamWriter sw = new StreamWriter(client.GetStream());
            NetworkStream ns = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Position pos = (Position)formatter.Deserialize(ns);
            pos.PositionY++;
            Console.WriteLine("Client Position : " + pos.PositionX + " , " + pos.PositionY);
            sw.WriteLine("Up");
            Console.WriteLine(sw.ToString());
            sw.Flush();
            foreach (KeyValuePair<int, TcpClient> Client in ClientList)
            {
                ns = Client.Value.GetStream();
                formatter.Serialize(ns, pos);
            }
        }

        public static void MovePlayerDown(TcpClient client)
        {
            StreamWriter sw = new StreamWriter(client.GetStream());
            NetworkStream ns = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Position pos = (Position)formatter.Deserialize(ns);
            pos.PositionY--;
            Console.WriteLine("Client Position : " + pos.PositionX + " , " + pos.PositionY);
            sw.WriteLine("Down");
            sw.Flush();
            foreach (KeyValuePair<int, TcpClient> Client in ClientList)
            {
                ns = Client.Value.GetStream();
                formatter.Serialize(ns, pos);
            }
        }

        public static void MovePlayerLeft(TcpClient client)
        {
            StreamWriter sw = new StreamWriter(client.GetStream());
            NetworkStream ns = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Position pos = (Position)formatter.Deserialize(ns);
            pos.PositionX--;
            Console.WriteLine("Client Position : " + pos.PositionX + " , " + pos.PositionY);
            sw.WriteLine("Left");
            sw.Flush();
            foreach (KeyValuePair<int, TcpClient> Client in ClientList)
            {
                ns = Client.Value.GetStream();
                formatter.Serialize(ns, pos);
            }
        }

        public static void MovePlayerRight(TcpClient client)
        {
            StreamWriter sw = new StreamWriter(client.GetStream());
            NetworkStream ns = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Position pos = (Position)formatter.Deserialize(ns);
            pos.PositionX++;
            Console.WriteLine("Client Position : " + pos.PositionX + " , " + pos.PositionY);
            sw.WriteLine("Right");
            sw.Flush();
            foreach (KeyValuePair<int, TcpClient> Client in ClientList)
            {
                ns = Client.Value.GetStream();
                formatter.Serialize(ns, pos);
            }
        }
        public void Main()
        {
            TcpListener listener = null;
            clientCounter = 0;
            try
            {
                listener = new TcpListener(IPAddress.Parse(IP_ADDRESS), PORT);
                listener.Start();

                Console.WriteLine("Server Start");

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Client Connected");
                    clientCounter++;
                    Thread newThread = new Thread(UloServer);
                    newThread.Start(client);
                }
            }

            catch (Exception a)
            {
                Console.WriteLine(a);
            }
        }

    }

}
