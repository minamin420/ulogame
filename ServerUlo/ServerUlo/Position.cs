﻿using System;

[Serializable]
    public class Position
    {
    private int posX = 0;
    private int posY = 0;

    public int PositionX
    {
        get { return posX; }
        set { posX = value; }
    }

    public int PositionY
    {
        get { return posY; }
        set { posY = value; }
    }

    public Position(int x, int y)
    {
        posX = x;
        posY = y;
    }
}
