﻿using System;

[Serializable]
public class User
{
    private string user;
    private string pass;

    public string Username
    {
        get { return user; }
        set { user = value; }
    }

    public string Password
    {
        get { return pass; }
        set { pass = value; }
    }

    public User(string username, string password)
    {
        user = username;
        pass = password;
    }
}