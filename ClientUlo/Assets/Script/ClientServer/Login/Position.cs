﻿using System;

[Serializable]
public class Position
{
    private int posX;
    private int posY;

    public int PositionX
    {
        get { return posX; }
        set { posX = value; }
    }

    public int PositionY
    {
        get { return posY; }
        set { posY = value; }
    }

    public Position(int x, int y)
    {
        posX = x;
        posY = y;
    }
}
