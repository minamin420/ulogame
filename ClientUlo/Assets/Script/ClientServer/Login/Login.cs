﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Reflection;

public class Login : MonoBehaviour
{
    public Text loginStatus;
    public InputField userInput, passInput;
    public Button playButton;
   
    private string playerName;
    private string sendUsername;

    private bool loggedIn;

    
    // Start is called before the first frame update
    void Start()
    {
        loggedIn = false;
        try
        {
            ConnectionDDOL.sr = new StreamReader(ConnectionDDOL.client.GetStream());
            ConnectionDDOL.sw = new StreamWriter(ConnectionDDOL.client.GetStream());
            Thread t = new Thread(ReceiveStatus);
            t.Start();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private void Update()
    {
        if (loggedIn)
        {
            loginStatus.text = "Welcome " + sendUsername;
            playButton.interactable = true;
        }
        
    }

    public static void SendString(string userString)
    {
        ConnectionDDOL.sw.WriteLine(userString);
        ConnectionDDOL.sw.Flush();
    }


    public void SendToServer(byte[] userByte)
    {
        ConnectionDDOL.ns.Write(userByte, 0, userByte.Length);
        ConnectionDDOL.ns.Flush();
    }

    public void LoginHandler()
    {
        playerName = userInput.text;
        User user = new User(userInput.text, passInput.text);
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(ConnectionDDOL.ns, user);
        loginStatus.text = "Welcome " + sendUsername;
        //Debug.Log(playerName);
    }

    void ReceiveStatus()
    {
        while (true)
        {
            sendUsername = ConnectionDDOL.sr.ReadLine();
            Debug.Log(sendUsername);
            if (sendUsername == playerName)
            {
                loggedIn = true;
                Debug.Log(sendUsername);
            }
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("TextBaseGame");
    }

    private void OnDisable()
    {
        
    }
    sealed class CustomizedBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type returntype = null;
            string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeName = typeName.Replace(sharedAssemblyName, assemblyName);
            returntype = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return returntype;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            base.BindToName(serializedType, out assemblyName, out typeName);
            assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }
    }
}


public static class ByteArraySerializer
{
    // from obj to byte 
    public static byte[] Serializer<T>(this T serializer)
    {
        using (var ms = new MemoryStream())
        {
            new BinaryFormatter().Serialize(ms, serializer);
            return ms.ToArray();
        }
    }

    // from byte to object
    public static T Deserializer<T>(this byte[] deserializer)
    {
        using (var ms = new MemoryStream(deserializer))
        {
            return (T)new BinaryFormatter().Deserialize(ms);
        }
    }
}