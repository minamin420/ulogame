﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int foodId;

    // Start is called before the first frame update
    public void Initialize(int _foodId)
    {
        foodId = _foodId;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Score.score++;
            Debug.Log(Score.score);
            FoodDestroy(foodId);
        }
    }
    public void FoodDestroy(int _foodId)
    {
        Destroy(GameManager.foods[_foodId].gameObject);
        GameManager.foods.Remove(_foodId);
    }
}
