﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class AuthManager : MonoBehaviour
{
    public static AuthManager instance;

    public TMP_InputField usernameInput;
    public TMP_InputField passwordInput;
    public TextMeshProUGUI statusText;
    public Button playButton;
    public GameObject panelLogin;
    public GameObject creditPanel;
    public GameObject scoreText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    void Start()
    {
        statusText.text = "";
    }

    public void ResetInput()
    {
        usernameInput.text = "";
        passwordInput.text = "";
    }
    public void Login()
    {
        ClientSend.LoginInput(usernameInput.text, passwordInput.text);
    }

    public void SignUp()
    {
        ClientSend.SignUpInput(usernameInput.text, passwordInput.text);
    }

    public void Play()
    {
        panelLogin.transform.localScale = new Vector3 (0, 0, 0);
        scoreText.transform.localScale = new Vector3 (1, 1, 1);
    }

    public void Credit()
    {
        creditPanel.transform.localScale = new Vector3(1, 1, 1);
    }

    public void BackFromCredit()
    {
        creditPanel.transform.localScale = new Vector3(0, 0, 0);
    }
}
