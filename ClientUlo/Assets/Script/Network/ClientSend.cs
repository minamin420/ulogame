using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }

    #region Packets
    public static void TCPTestReceived()
    {
        using (Packet _packet = new Packet((int)ClientPackets.tcpTestReceived))
        {
            _packet.Write(Client.instance.myId);
            
            SendTCPData(_packet);
        }
    }

    public static void LoginInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.loginInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

    public static void SignUpInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.signUpInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

    public static void PlayerMovement(bool[] _inputs)
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Client.instance.myId].transform.rotation);
            SendUDPData(_packet);
        }
    }
    #endregion
}
