﻿using System.Text;
using UnityEngine;

public class ServerSend
{
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }

    #region Packets
    public static void TCPTest(int _toClient)
    {
        using (Packet _packet = new Packet((int)ServerPackets.tcpTest))
        {
            _packet.Write(_toClient);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void UDPTest(int _toClient)
    {
        using (Packet _packet = new Packet((int)ServerPackets.udpTest))
        {
            SendUDPData(_toClient, _packet);
        }
    }

    public static void Login(int _id, bool _value, string _message)
    {
        using (Packet _packet = new Packet((int)ServerPackets.login))
        {
            _packet.Write(_value);
            _packet.Write(_message);

            if(_value)
            {
                _packet.Write(Server.clients[_id].username);
                Debug.Log("Player id is logged in, welcome " + Server.clients[_id].username);
            }

            SendTCPData(_id, _packet);
        }
    }

    public static void SignUp(int _id, bool _value, string _message)
    {
        using (Packet _packet = new Packet((int)ServerPackets.signUp))
        {
            _packet.Write(_value);
            _packet.Write(_message);

            SendTCPData(_id, _packet);
        }
    }

    public static void PlayerDisconnected(int _id)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_id);

            SendTCPDataToAll(_id, _packet);
        }
    }

    public static void PlayerConnect(int _id)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerConnect))
        {
            _packet.Write(_id);

            SendTCPDataToAll(_id, _packet);
        }
    }

    public static void playerSpawn(int _id, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerSpawn))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            Debug.Log(_player.transform.position);
            SendTCPData(_id, _packet);
        }
    }

    public static void PlayerPosition(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerRotation(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }
    public static void spawnFood(int _foodId, Vector3 _position)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnFood))
        {
            _packet.Write(_foodId);
            _packet.Write(_position);
            SendTCPDataToAll(_packet);
        }
    }

    public static void destroyFood(int _foodId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.destroyFood))
        {
            _packet.Write(_foodId);
            SendTCPDataToAll(_packet);
        }
    }
    #endregion
}
