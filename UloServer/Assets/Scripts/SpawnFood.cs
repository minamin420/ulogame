﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFood : MonoBehaviour
{
    public static Dictionary<int, Food> food = new Dictionary<int, Food>();
    private int idCount;

    public Vector3 spawnValues;

    public float spawnMostWait;
    public float spawnLeastWait;
    private float spawnWait;

    [Header("Prefab")]
    public Food foodPrefab;

    private void Start()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    private void Update()
    {
        if (spawnWait > 0)
        {
            spawnWait -= Time.deltaTime;
        }
        else
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y), 0);
            spawnPosition += this.transform.position;

            ServerSend.spawnFood(idCount, spawnPosition);
            Food food = Instantiate(foodPrefab, spawnPosition, foodPrefab.transform.rotation) as Food;

            food.Initialize(idCount);
            idCount++;

            //food.Add(idCount, food);

            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }

    }

}
